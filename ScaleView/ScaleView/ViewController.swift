//
//  ViewController.swift
//  ScaleView
//
//  Created by Dejan Kraguljac on 18/12/2017.
//  Copyright © 2017 Dejan Kraguljac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var scrollableView: ScrollableScaleView!
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAndDrawScaleView()

    }
    
    func setupAndDrawScaleView() {
        self.scrollableView.lineWidth = 1
        self.scrollableView.indicatorWidth = CGFloat(3)
        self.scrollableView.linesSpacing = 5
        self.scrollableView.indicatorColor = .black
        self.scrollableView.usedLinesColor = .blue
        self.scrollableView.minValue = 10
        self.scrollableView.maxValue = 500
        self.scrollableView.unusedLinesColor = UIColor.blue.withAlphaComponent(0.54)
        self.scrollableView.shouldShowHorizontalScrollIndicator = false
        self.scrollableView.scaleLabelColor = self.scrollableView.usedLinesColor
        self.scrollableView.numberOfUsedLines = Int(self.scrollableView.maxValue - self.scrollableView.minValue)
        self.scrollableView.system = .metric
        self.scrollableView.useWeightScale = false
        
        self.scrollableView.gotValueUsingOffset = { [weak self] value in
            guard let `self` = self else { return }
            
            self.label.text = "\(value)"
        }
        self.scrollableView.drawScaleView { (_) in
            
        }
    }
    
}

