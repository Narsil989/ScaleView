//
//  Units+Extensions.swift
//  ScaleView
//
//  Created by Dejan Kraguljac on 02/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import UIKit

/// This extension file is for units only e.g. for converting values from metric -> implerial, implerial -> metric, cm -> m, etc...
extension Double {
    
    // cm -> m
    var cmToM: Double {
        return self / 100
    }
    
    // cm -> in
    var cmToIn: Double {
        return self * 0.39370
    }
    
    // in -> cm
    var inToCm: Double {
        return self / 0.39370
    }
    
    // lbs -> kg
    var lbsToKg: Double {
        return self / 2.2046
    }
    
    // kg -> lbs
    var kgToLbs: Double {
        return self * 2.2046
    }
    
}

