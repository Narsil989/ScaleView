

import UIKit

open class ScrollableScaleView: UIView {

    var scaleContainerView: UIView = {
        let view = UIView(frame: CGRect.zero)
        
        return view
    }()
    
    @IBOutlet weak var indicatorImageView: UIImageView!
    @IBOutlet weak var scaleScrollView: UIScrollView!
    @IBOutlet weak var indicatorImageViewWidthConstraint: NSLayoutConstraint!
    
    var numberOfUsedLines: Int = 10
    var minValue: Double = 0
    var maxValue: Double = 0
    
    var system: MeasurementSystem = Locale.current.usesMetricSystem ? .metric : .imperial {
        didSet {
            if self.system == .imperial && !self.useWeightScale {
                self.scalingValue = 6
            } else {
                self.scalingValue = 5
            }
            self.reloadScaleData()
        }
    }
    
    var isDrawing = false
    var lineWidth = 1
    var usedLinesColor = UIColor.black
    var unusedLinesColor = UIColor.lightGray
    var indicatorWidth: CGFloat = 1
    var linesSpacing = 4
    var scalingValue = 5
    var scaleLabelFont = UIFont(name: "Futura", size: 8)
    var scaleLabelColor = UIColor.green
    var numberOfUnusedLines: Int = Int(UIScreen.main.bounds.size.width/2)/5
    var mainLinesLabelPercentHeightValue = 0.5
    var subLinesLabelPercentHeightValue = 0.75
    
    var gotValueUsingOffset: ((_ currentValue: Double) -> Void)?
    
    var scaleLabels: [UILabel] = [UILabel]()
        
    var unusedLinesOffset: Double = 0 {
        didSet {
            self.indicatorImageViewWidthConstraint.constant = self.indicatorWidth
        }
    }
    
    var indicatorColor: UIColor = UIColor.blue {
        didSet {
            self.indicatorImageView.backgroundColor = self.indicatorColor
        }
    }

    var linesWithSpaceWidth: Int {
        let width = self.lineWidth + self.linesSpacing
        return width
    }
    var shouldShowHorizontalScrollIndicator = false {
        didSet {
            self.scaleScrollView.showsHorizontalScrollIndicator = shouldShowHorizontalScrollIndicator
        }
    }
    
    var useBounces = false {
        didSet {
            self.scaleScrollView.bounces = useBounces
        }
    }
    
    //this bool only because of feet scale... custom label values...
    var useWeightScale: Bool = true
    
    // MARK:init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ScrollableScaleView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
//        self.drawScaleView()
    }
    
    func drawScaleView(withCompletion completion: ((Bool) -> Void))
    {
        guard self.frame != .zero,
            self.minValue < self.maxValue,
            self.minValue >= 0,
            self.maxValue >= 0,
            self.linesSpacing > 0,
            self.isDrawing == false
            else {
                return
        }
        
        self.resetScaleView()
        self.isDrawing = true
        self.indicatorImageView.backgroundColor = self.indicatorColor
        self.indicatorImageViewWidthConstraint.constant = self.indicatorWidth
        self.numberOfUnusedLines = Int(UIScreen.main.bounds.size.width/2)/linesWithSpaceWidth
        let unusedLinesMultiplier: Int = self.scaleScrollView.bounces == true ? 4 : 2
        
        //DispatchQueue.global().async {
            
        for index in 0 ... (self.numberOfUnusedLines + self.numberOfUsedLines*unusedLinesMultiplier) {
            
            self.drawScaleLine(offset: self.linesWithSpaceWidth*(index) + Int(UIScreen.main.bounds.size.width/2), color:(index > self.numberOfUsedLines) ? self.unusedLinesColor : self.usedLinesColor, addLabel:self.numberOfUsedLines >= index, bigLine: (index + Int(self.minValue))%self.scalingValue == 0)
        }
        for index in 1 ... self.numberOfUnusedLines*unusedLinesMultiplier {
            
            self.drawScaleLine(offset: Int(UIScreen.main.bounds.size.width/2) - self.linesWithSpaceWidth*(index), color:self.unusedLinesColor, addLabel: false, bigLine: (index + self.scalingValue - Int(self.minValue))%self.scalingValue == 0)
            
            if index >= self.numberOfUnusedLines*unusedLinesMultiplier
            {
                completion(true)
            }
        }
            
        
        //}
        self.scaleContainerView.setNeedsDisplay()
        self.scaleContainerView.layoutIfNeeded()
        
        self.scaleContainerView.frame = CGRect(x: 0, y: 0, width: linesWithSpaceWidth*numberOfUsedLines + Int(UIScreen.main.bounds.size.width), height: Int(self.frame.size.height))
        
        scaleScrollView.addSubview(self.scaleContainerView)
        scaleScrollView.contentSize = self.scaleContainerView.frame.size
        
    
    }
    
    // MARK:draw lines
    
    fileprivate func drawScaleLine(offset: Int, color: UIColor, addLabel: Bool, bigLine: Bool) {
        let path = UIBezierPath()
        let shapeLayer = CAShapeLayer()
        
        path.move(to: CGPoint(x: offset, y: Int(self.frame.size.height)))
        
        if bigLine == false {
            path.addLine(to: CGPoint(x: offset, y: Int(Double(self.frame.size.height)*self.subLinesLabelPercentHeightValue)))
        }
        else {
            path.addLine(to: CGPoint(x: offset, y: Int(Double(self.frame.size.height)*self.mainLinesLabelPercentHeightValue)))
            
            if addLabel == true {
                let label = UILabel(frame: CGRect(x: offset-10, y: Int(Double(self.frame.size.height) - 10.0 - Double(self.frame.size.height)*(1.0-self.mainLinesLabelPercentHeightValue)), width: 20, height: Int((self.scaleLabelFont?.pointSize)!)))
                label.text = self.getScaleLabelTextForSystem(forOffset: offset)
                label.textAlignment = .center
                label.font = self.scaleLabelFont
                label.textColor = self.scaleLabelColor
                self.scaleContainerView.addSubview(label)
                
                self.scaleLabels.append(label)
            }
        }
        
        shapeLayer.path = path.cgPath
        shapeLayer.lineWidth = CGFloat(self.lineWidth)
        shapeLayer.strokeColor = color.cgColor
        scaleContainerView.layer.addSublayer(shapeLayer)
    }
    
    // MARK:get label text,  added this because of custom label values
    
    func getScaleLabelTextForSystem(forOffset offset: Int) -> String {
        if (system == .metric || self.useWeightScale) {
            return "\(Int(minValue) + offset/self.linesWithSpaceWidth - self.numberOfUnusedLines)"
        }
        else if (self.system == .imperial && !self.useWeightScale) {
            
            let mainValue : Int = (Int(self.minValue) + offset/self.linesWithSpaceWidth - self.numberOfUnusedLines)/12
            let additionalValue: Int = (Int(self.minValue) + offset/self.linesWithSpaceWidth - self.numberOfUnusedLines)%12 < 6 ? 0 : 6
            return "\(mainValue)'" + "\(additionalValue)\""
        }
        else {
            return ""
        }
    }
    
    // MARK: feature  scroll to entered value
    func scrollToValue(value: Double) {
        guard value <= self.maxValue, value >= self.minValue else {
            return
        }
        
        let newOffset = self.getOffsetForValue(value: value)
        
        UIView.animate(withDuration: 1,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: {
                        
                        self.scaleScrollView.contentOffset = CGPoint(x: Int(newOffset), y: 0)
                        
        }, completion: nil)
    }
    
    
    // MARK:helpers
    
    public func getOffsetForValue(value: Double) -> Double {
        guard value <= self.maxValue  else {
            return Double(self.linesWithSpaceWidth)*self.maxValue
        }
        return Double(self.linesWithSpaceWidth)*(value - self.minValue)
    }

    
    func resetScaleView() {
        
        self.scaleContainerView.removeFromSuperview()
        self.scaleLabels.removeAll()
        
        guard self.scaleContainerView.layer.sublayers != nil else {
            return
        }
        
        for scaleLayer in self.scaleContainerView.layer.sublayers! {
            if scaleLayer is CAShapeLayer {
                scaleLayer.removeFromSuperlayer()
            }
            
        }
        
        for scaleLabel in self.scaleContainerView.subviews {
            if scaleLabel is UILabel
            {
                scaleLabel.removeFromSuperview()
            }
        }

    }
    
    func reloadScaleData() {
        
        self.scaleContainerView.setNeedsDisplay()
        self.scaleContainerView.layoutIfNeeded()
        self.drawScaleView() { finished in
        
            self.isDrawing = false
        
        }
    }
    
    func updateScrollViewWith(newMin min: Int, newMax max: Int) {
        guard min > 0, min < max
            else {
            return
        }
        
        self.minValue = Double(min)
        self.maxValue = Double(max)
        self.numberOfUsedLines = (max-min)
    }
    
    func updateScaleLabelsWith(newFont font: UIFont = UIFont(name: "HelveticaNeue", size: 12)!, newColor color: UIColor = UIColor.blue.withAlphaComponent(0.12)) {
        for label in self.scaleLabels {
            label.textColor = color
            label.font = font
            label.frame = CGRect(x:Int(label.frame.origin.x), y: Int(Double(self.frame.size.height) - Double(font.pointSize + 2.0) - Double(self.frame.size.height)*(1.0-self.mainLinesLabelPercentHeightValue)), width: 20, height: Int(font.pointSize))
            //+2 so the label wont be stuck on the line
        }
    }
    
    func configureScrollMeasure(unit: Unit, defaultValue: Double) {
        
        self.minValue = unit.minValue
        self.maxValue = unit.maxValue
        
        self.numberOfUsedLines = Int(self.maxValue - self.minValue)
        
        let newOffset = self.getOffsetForValue(value: defaultValue)
        self.scaleScrollView.setContentOffset(CGPoint(x: newOffset, y: 0), animated: false)
        
        switch unit {
        case .mass(system: let measureSystem):
            self.useWeightScale = true
            self.system = measureSystem
        case .lenght(system: let measureSystem):
            self.useWeightScale = false
            self.system = measureSystem
        }
        
        let scrollOffsetPercentage: Double = (Double(self.scaleScrollView.contentOffset.x)/Double(self.scaleScrollView.contentSize.width - UIScreen.main.bounds.size.width))
        let value = self.minValue + (self.maxValue - self.minValue)*scrollOffsetPercentage
        if let block = self.gotValueUsingOffset {
            block(value)
        }
    }
}

extension ScrollableScaleView: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollOffsetPercentage: Double = (Double(scrollView.contentOffset.x)/Double(self.scaleScrollView.contentSize.width - UIScreen.main.bounds.size.width))
        
        let value = self.minValue + (self.maxValue - self.minValue)*scrollOffsetPercentage
        
        if let block = self.gotValueUsingOffset, value >= self.minValue, value <= self.maxValue {
            block(value)
        }
        else if let block = self.gotValueUsingOffset, value < self.minValue {
            block(self.minValue)
        }
        else if let block = self.gotValueUsingOffset, value > self.maxValue {
            block(self.maxValue)
        }
    }
}


