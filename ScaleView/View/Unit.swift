//
//  Unit.swift
//  ScaleView
//
//  Created by Dejan Kraguljac on 02/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

import UIKit

enum Unit {
    case mass(system: MeasurementSystem)
    case lenght(system: MeasurementSystem)
    
    var symbol: String {
        switch self {
        case .mass(system: let system):
            return system == .metric ? "kg" : "lbs"
        case .lenght(system: let system):
            return system == .metric ? "cm" : "feet"
        }
    }
    
    var maxValue: Double {
        switch self {
        case .mass(system: let system):
            return system == .metric ? 300 : 661
        case .lenght(system: let system):
            return system == .metric ? 270 : 106
        }
    }
    
    var minValue: Double {
        switch self {
        case .mass(system: let system):
            return system == .metric ? 40 : 88
        case .lenght(system: let system):
            return system == .metric ? 120 : 47
        }
    }
    
    func getValueString(value:Double, withSymbol:Bool = true) -> NSAttributedString {
        
        switch self {
        case .lenght(system: let measureSystem):
            if measureSystem == .metric {
                let roundValue = Int(round(value))
                if !withSymbol {
                    return NSAttributedString(string: "\(roundValue)")
                    
                }
                let title = "\(Int(value)) \(self.symbol)"
                
                return NSAttributedString(string: title)
            } else {
                let feet = value/12
                let inches = Int(round(feet.truncatingRemainder(dividingBy: 1) * 12))
                if !withSymbol {
                    return NSAttributedString(string: "\(Int(feet))'\(inches)\"")
                    
                }
                let title = "\(Int(feet))'\(inches)\" \(self.symbol)"
                
                return NSAttributedString(string: title)
            }
        case .mass(system: _):
            let roundValue = Int(round(value))
            if !withSymbol {
                return NSAttributedString(string: "\(roundValue)")
                
            }
            
            let title = "\(roundValue) \(self.symbol)"
            return NSAttributedString(string: title)
            
        }
    }
    
    
}
