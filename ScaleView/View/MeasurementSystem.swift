//
//  MeasurementSystem.swift
//  ScaleView
//
//  Created by Dejan Kraguljac on 02/01/2018.
//  Copyright © 2018 Dejan Kraguljac. All rights reserved.
//

enum MeasurementSystem: String {
    case imperial
    case metric
    
    init?(index:Int) {
        switch index {
        case 0:
            self = .metric
        case 1:
            self = .imperial
        default: return nil
        }
    }
    
    var title:String {
        switch self {
        case .imperial:
            return "Imperial"
        case .metric:
            return "Metric"
        }
    }
    
    //added for posibility of changed on backend. Used for set values in parameters on network request
    var rawValue:String {
        switch self {
        case .imperial:
            return "imperial"
        case .metric:
            return "metric"
        }
    }
    
    var index:Int {
        switch self {
        case .metric:
            return 0
        case .imperial:
            return 1
        }
    }
}
